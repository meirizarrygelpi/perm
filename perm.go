// Copyright (c) 2015-2017 Melvin Eloy Irizarry-Gelpí.
// Licensed under the MIT License.

package perm

import (
	"strconv"
	"strings"

	"bitbucket.org/meirizarrygelpi/shuffle"
)

// A Perm type represents a collection of objects labeled by integers.
type Perm []int

// Len method returns the integer number of items in the permutation.
func (p Perm) Len() int {
	return len(p)
}

// Swap method swaps the ith and jth entry in the permutation.
func (p Perm) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}

// String method returns the string representation of a Perm as a sequence of
// integers separated by dashes.
func (p Perm) String() string {
	a := make([]string, p.Len())
	for i, n := range p {
		a[i] = strconv.Itoa(n)
	}
	return strings.Join(a, "-")
}

// Identity function returns the identity permutation.
func Identity(n int) Perm {
	p := make(Perm, n)
	for i := range p {
		p[i] = i
	}
	return p
}

// ShiftLeft method returns a permutation made by cyclically-shifting all
// elements of a given permutation to the left.
func (p Perm) ShiftLeft() Perm {
	q := make(Perm, len(p))
	copy(q, p[1:])
	q[len(p)-1] = p[0]
	return q
}

// ShiftRight method returns a permutation made by cyclically-shifting all
// elements of a given permutation to the right.
func (p Perm) ShiftRight() Perm {
	q := make(Perm, len(p))
	copy(q[1:], p)
	q[0] = p[len(p)-1]
	return q
}

// Mirror method returns a permutation made by completely inverting a given
// permutation.
func (p Perm) Mirror() Perm {
	q := make(Perm, len(p))
	for i := range p {
		q[i] = p[len(p)-1-i]
	}
	return q
}

// Shuffle method returns a permutation made by randomly shuffling a given
// permutation.
func (p Perm) Shuffle() Perm {
	k := len(p)
	q := make(Perm, k)
	shuffle.FisherYates(p)
	for i, j := range p {
		q[i] = j
	}
	return q
}

// AllLeftShifts method returns a channel that receives all of the cyclic left
// shifts of a given permutation.
func (p Perm) AllLeftShifts() <-chan Perm {
	out := make(chan Perm)
	go func() {
		defer close(out)
		for i := 0; i < len(p); i++ {
			out <- p
			p = p.ShiftLeft()
		}
	}()
	return out
}

// AllRightShifts method returns a channel that receives all of the cyclic right
// shifts of a given permutation.
func (p Perm) AllRightShifts() <-chan Perm {
	out := make(chan Perm)
	go func() {
		defer close(out)
		for i := 0; i < len(p); i++ {
			out <- p
			p = p.ShiftRight()
		}
	}()
	return out
}

// CyclicInequivalent function returns a channel that receives all of the
// cyclic-inequivalent permutations of a given length.
func CyclicInequivalent(k int) <-chan Perm {
	out := make(chan Perm)
	if k == 1 {
		go func() {
			defer close(out)
			out <- Perm{0}
		}()
		return out
	}
	go func() {
		defer close(out)
		for p := range CyclicInequivalent(k - 1) {
			for q := range p.AllLeftShifts() {
				out <- append(q, k-1)
			}
		}
	}()
	return out
}

// MirrorCyclicInequivalent function returns a channel that receives all of the
// mirror- and cyclic-inequivalent permutations of a given length.
func MirrorCyclicInequivalent(k int) <-chan Perm {
	out := make(chan Perm)
	if k == 1 {
		go func() {
			defer close(out)
			out <- Perm{0}
		}()
		return out
	}
	if k == 2 {
		go func() {
			defer close(out)
			out <- Perm{0, 1}
		}()
		return out
	}
	if k == 3 {
		go func() {
			defer close(out)
			out <- Perm{0, 1, 2}
		}()
		return out
	}
	go func() {
		defer close(out)
		for p := range MirrorCyclicInequivalent(k - 1) {
			for q := range p.AllLeftShifts() {
				out <- append(q, k-1)
			}
		}
	}()
	return out
}

// All function returns a channel receiving all permutations of a given length.
func All(k int) <-chan Perm {
	out := make(chan Perm)
	go func() {
		defer close(out)
		for p := range CyclicInequivalent(k) {
			for q := range p.AllLeftShifts() {
				out <- q
			}
		}
	}()
	return out
}

// merge function.
func merge(a, b Perm) Perm {
	la, lb := len(a), len(b)
	i, j := 0, 0
	n := la + lb
	c := make(Perm, n)
	for k := 0; k < n; k++ {
		if (i < la) && (j < lb) {
			if a[i] < b[j] {
				c[k] = a[i]
				i++
			} else {
				c[k] = b[j]
				j++
			}
		} else if i > la-1 {
			c[k] = b[j]
			j++
		} else {
			c[k] = a[i]
			i++
		}
	}
	return c
}

// Sort method.
func (p Perm) Sort() Perm {
	n := p.Len()
	if (n == 0) || (n == 1) {
		return p
	}
	h := n / 2
	a := p[:h].Sort()
	b := p[h:].Sort()
	return merge(a, b)
}

// mergeAndCountSplitInv function.
func mergeAndCountSplitInv(a, b Perm) (Perm, int64) {
	la, lb := len(a), len(b)
	i, j := 0, 0
	n := la + lb
	var inv int64
	c := make(Perm, n)
	for k := 0; k < n; k++ {
		if (i < la) && (j < lb) {
			if a[i] < b[j] {
				c[k] = a[i]
				i++
			} else {
				c[k] = b[j]
				j++
				inv = inv + int64(la) - int64(i)
			}
		} else if i > la-1 {
			c[k] = b[j]
			j++
		} else {
			c[k] = a[i]
			i++
		}
	}
	return c, inv
}

// sortAndCountInv function.
func sortAndCountInv(p Perm) (Perm, int64) {
	n := p.Len()
	if (n == 0) || (n == 1) {
		return p, 0
	}
	h := n / 2
	a, x := sortAndCountInv(p[:h])
	b, y := sortAndCountInv(p[h:])
	c, z := mergeAndCountSplitInv(a, b)
	return c, x + y + z
}

// Inversions method returns the number of inversions in a given permutation.
func (p Perm) Inversions() int64 {
	_, inv := sortAndCountInv(p)
	return inv
}

// Sign method returns 1 if a given permutation has an even number of
// inversions, -1 otherwise.
func (p Perm) Sign() int {
	if (p.Inversions() % 2) == 0 {
		return 1
	}
	return -1
}

// AllEven function returns a channel receiving all even permutations of a given
// length.
func AllEven(k int) <-chan Perm {
	out := make(chan Perm)
	go func() {
		defer close(out)
		for p := range All(k) {
			if p.Sign() == 1 {
				out <- p
			}
		}
	}()
	return out
}

// AllOdd function returns a channel receiving all odd permutations of a given
// length.
func AllOdd(k int) <-chan Perm {
	out := make(chan Perm)
	go func() {
		defer close(out)
		for p := range All(k) {
			if p.Sign() == -1 {
				out <- p
			}
		}
	}()
	return out
}
